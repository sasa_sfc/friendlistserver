class CreateFriendRelations < ActiveRecord::Migration
  def change
    create_table :friend_relations do |t|
      t.integer :user_id
      t.string :facebook_uid
      t.integer :friend_id

      t.timestamps
    end
  end
end
