class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :facebook_uid
      t.string :name
      t.string :cover_url

      t.timestamps
    end
  end
end
