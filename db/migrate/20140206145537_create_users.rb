class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :facebook_uid
      t.string :email
      t.string :access_token
      t.string :name
      t.string :gender
      t.integer :old
      t.string :icon_url
      t.string :cover_url

      t.timestamps
    end
  end
end
