class ApiController < ApplicationController
  def getgroups
    groups = User.where(id: params[:user_id]).first.groups
    render json: groups.to_json
  end

  def getmember
    member = Group.where(id: params[:group_id]).first.users
    render json: member.to_json

  end
end
