class LoginController < ApplicationController
  def test
    #@token = "CAACEdEose0cBAFcY4d9MiJWrxsZCmpOBIB2cmkmiUZBvDq2Rc49HVLkwY38ZBpaDUQw70e9xS7mX29hF1e2Bg8CWwSmpCUpeVFbIHhtHfJcmZCORVA5FDtk6ArPlo5LAPIsEgTiKS3oAgdEZC2h6XrZAiFmqb0yjQSiSiWsxOpeXCQLh6YsuvJgmnq8GkQZAl4ZD"
    @token = params[:token]
    @graph = Koala::Facebook::API.new(@token)
    facebook_uid = @graph.get_object("me")["id"]

    # userの作成
    self.create_user_and_friends(facebook_uid)
    @user_id = User.where(facebook_uid: facebook_uid).first.id

    #friend情報の作成
    self.create_friend_relation(facebook_uid)

    #Group情報の作成
    self.create_group(facebook_uid)
    self.create_group_relation(facebook_uid)


    render text:  "Hello world"
  end

  def create_user_and_friends(facebook_uid)
    we = @graph.get_object("#{facebook_uid}?fields=friends.fields(id,name,gender,picture.width(100).height(100).type(square)),id,gender,name,picture.width(100).height(100).type(square)", locale: 'ja_JP')
    new_user = User.new
    new_user.facebook_uid = we["id"]
    new_user.name = we["name"]
    new_user.gender = we["gender"]
    new_user.icon_url = we["picture"]["data"]["url"]
    new_user.save

    we["friends"]["data"].each do |friend|
      new_user = User.new
      new_user.facebook_uid = friend["id"]
      new_user.name = friend["name"]
      new_user.gender = friend["gender"]
      new_user.icon_url = friend["picture"]["data"]["url"]
      new_user.save
    end

  end

  def create_friend_relation(facebook_uid)
    friends = @graph.get_object("#{facebook_uid}?fields=friends.fields(id)", locale: 'ja_JP')
    friends["friends"]["data"].each do |friend|
      new_relation = FriendRelation.new
      new_relation.user_id = @user_id
      new_relation.friend_id = User.where(facebook_uid: friend["id"]).first.id
      new_relation.save
    end
  end

  def create_group(facebook_uid)
    groups = @graph.get_object("#{facebook_uid}?fields=groups.fields(name,id,cover)")
    groups["groups"]["data"].each do |group|
      new_group = Group.new
      new_group.facebook_uid = group["id"]
      new_group.name = group["name"]
      if group["cover"].present?
        new_group.cover_url = group["cover"]["source"]
      end
      new_group.save
    end
  end

  def create_group_relation(facebook_uid)
    groups = @graph.get_object("#{facebook_uid}?fields=groups.fields(members)")
    groups["groups"]["data"].each do |group|
      group_id = Group.where(facebook_uid: group["id"]).first.id rescue binding.pry
      group["members"]["data"].each do |member|
        new_relation = GroupRelation.new
        new_relation.group_id = group_id
        if User.where(facebook_uid: member["id"]).first.present?
          new_relation.user_id = User.where(facebook_uid: member["id"]).first.id
          new_relation.save
        end
      end
    end
  end
end
