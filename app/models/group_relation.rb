# == Schema Information
#
# Table name: group_relations
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  group_id   :integer
#  created_at :datetime
#  updated_at :datetime
#

class GroupRelation < ActiveRecord::Base
  validates_uniqueness_of :user_id, scope: :group_id

  belongs_to :group
  belongs_to :user

end
