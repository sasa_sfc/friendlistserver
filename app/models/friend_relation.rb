# == Schema Information
#
# Table name: friend_relations
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  facebook_uid :string(255)
#  friend_id    :integer
#  created_at   :datetime
#  updated_at   :datetime
#

class FriendRelation < ActiveRecord::Base
  belongs_to :user
  belongs_to :friend, class_name: "User"
  validates_uniqueness_of :user_id, scope: :friend_id
end
