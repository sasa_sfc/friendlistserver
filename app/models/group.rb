# == Schema Information
#
# Table name: groups
#
#  id           :integer          not null, primary key
#  facebook_uid :string(255)
#  name         :string(255)
#  cover_url    :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#

class Group < ActiveRecord::Base
  validates_uniqueness_of :facebook_uid

  has_many :group_relations
  has_many :users, through: :group_relations, class_name: 'User'
end
