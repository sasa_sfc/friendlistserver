# == Schema Information
#
# Table name: users
#
#  id           :integer          not null, primary key
#  facebook_uid :string(255)
#  email        :string(255)
#  access_token :string(255)
#  name         :string(255)
#  gender       :string(255)
#  old          :integer
#  icon_url     :string(255)
#  cover_url    :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#

class User < ActiveRecord::Base
  validates_uniqueness_of :facebook_uid
  has_many :friend_relations
  has_many :friends, through: :friend_relations, class_name: 'User', foreign_key: :friend_id

  has_many :group_relations
  has_many :groups, through: :group_relations, class_name: 'Group', foreign_key: :group_id


end
